@extends('master')

@section('contant')

<section class="register">
	<div class="container ">
        <div class="row">
            <div class="col-md-2 register-left">
                <!-- <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/> -->
                <h1>Welcome</h1>
                <a href="{{route('login')}}">
                    <button class="btn btn-success">
                        <i class="fas fa-sign-in-alt"></i>  Sign In
                    </button>
                </a>
            </div>
            <div class="col-md-7 register-right">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <h1 class="register-heading">Apply as a Employee or Job Seeker</h1>
                        <div class="row register-form">
                            <div class="col-md-12">
                            @if (session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session('message') }}
                                </div>
                            @endif
                            	<form action="{{route('registration')}}" method="post" enctype="multipart/form-data">
                                    @csrf
	                                <div class="form-group col-md-6">
	                                	<label for="first_name">First Name</label>
	                                    <input type="text" id="first_name" class="form-control" name="first_name" placeholder="First Name *" value="{{ old('first_name') }}" />
                                        @if($errors->has('f_name'))
                                            <span class="label label-danger">
                                                {{ $errors->first('f_name') }}
                                            </span>
                                        @endif
	                                </div>
	                                <div class="form-group col-md-6">
	                                	<label for="last_name">Last Name</label>
	                                    <input type="text" id="last_name" class="form-control" placeholder="Last Name *" name="last_name" value="{{ old('l_name') }}" />
                                        @if($errors->has('l_name'))
                                            <span class="label label-danger">
                                                {{ $errors->first('l_name') }}
                                            </span>
                                        @endif
	                                </div>
	                                <div class="form-group col-md-6">
	                                	<label for="password">Password</label>
	                                    <input type="password" id="password" class="form-control" placeholder="Password *" name="password" value="{{ old('password') }}" />
                                        @if($errors->has('password'))
                                            <span class="label label-danger">
                                                {{ $errors->first('password') }}
                                            </span>
                                        @endif
	                                </div>
	                                <div class="form-group col-md-6">
	                                	<label for="confirm_password">Confirm Password</label>
	                                    <input type="password" id="confirm_password" class="form-control"  placeholder="Confirm Password *" value="" />
	                                </div>
	                                <div class="form-group">
	                                	<label for="email-address">Email</label>
	                                    <input type="email" id="email" class="form-control" placeholder="Your Email *" name="email" value="{{ old('email') }}" />
                                        @if($errors->has('email'))
                                            <span class="label label-danger">
                                                {{ $errors->first('email') }}
                                            </span>
                                        @endif
	                                </div>
	                                <div class="form-group">
	                                	<label for="phone">Phone</label>
	                                    <input type="tel" id="phone" minlength="10" maxlength="15" class="form-control" placeholder="Your Phone *" name="phone" value="{{ old('phone') }}" />
                                        @if($errors->has('phone'))
                                            <span class="label label-danger">
                                                {{ $errors->first('phone') }}
                                            </span>
                                        @endif
	                                </div>
	                                <div class="form-group">
                                        <label for="user_name">User Name</label>
                                        <input type="text" id="user_name" class="form-control" placeholder="User Name *" name="user_name" value="{{ old('user_name') }}" />
                                        @if($errors->has('user_name'))
                                            <span class="label label-danger">
                                                {{ $errors->first('user_name') }}
                                            </span>
                                        @endif
                                    </div>
                                     <div class="form-group">
	                                	<label for="">User Type</label>
	                                    <select name="user_type" class="form-control input-lg">
                                            <option value="employer">Employers</option>
                                            <option value="job_seeker">Job Seeker</option>
                                        </select>
                                        @if($errors->has('user_type'))
                                            <span class="label label-danger">
                                                {{ $errors->first('user_type') }}
                                            </span>
                                        @endif
	                                </div>
	                                <input type="submit" class="btnRegister"  value="Register"/>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 register-widget">
                <div class="features-area">
                    <ul>
                        <li>
                            <img src="http://mybdjobs.bdjobs.com/mybdjobs/images/ficon_01.png" alt="">
                            <p>
                                Get instant Notification about New job Posting, Match Jobs and More.
                            </p>
                        </li>

                        <li>
                            <img src="http://mybdjobs.bdjobs.com/mybdjobs/images/ficon_02.png" alt="">
                            <p class="lipad">
                                Your world of Jobs on the go. Search, View, Apply to any jobs from anywhere.
                            </p>
                        </li>

                        <li>
                            <img src="http://mybdjobs.bdjobs.com/mybdjobs/images/ficon_03.png" alt="">
                            <p>
                                Receive Messages from Employers and Increase your Possibility.
                            </p>
                        </li>

                        <li style="margin-bottom: 0px">
                            <img src="http://mybdjobs.bdjobs.com/mybdjobs/images/ficon_04.png" alt="">
                            <p class="">
                                Join 10,000+ Companies who have posted more than 1,50,000+ Jobs!
                            </p>
                        </li>
                    </ul>
                </div>
                
                <div class="help-area">
                    <h2>
                      Need Help?
                    </h2>

                    <p>
                        If you are facing any problem and have any query then feel free to ask.
                    </p>

                    <h4 class="phone-number">
                      <i class="glyphicon glyphicon-phone"></i>
                      xxxxxxxxxx
                    </h4>

                    <h4 class="email-address">
                      <i class="glyphicon glyphicon-envelope"></i>
                       support@llc.com
                    </h4>
                </div>
                
                
                <div class="aregister">
                    <h2>
                      Already Registered?
                    </h2>
                    <a href="{{route('login')}}">
                        <button type="button" class="btn signin-btn log-in">
                            Sign In Now
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

@stop


