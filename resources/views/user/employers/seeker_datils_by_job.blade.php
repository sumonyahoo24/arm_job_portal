@extends('user.employers.master')

@section('contant')

<div class="row bottom-mrg full-detail-description seeker_details_by_heading">
	<h1 class="detail-title seeker_details_by_title">Resume</h1>
</div>
<div class="row bottom-mrg col_text_center">
	<div class="col-md-4 col-sm-4">
		<div class="get-touch">
			<h4>Get in Touch</h4>
			<ul>
				<li><i class="fa fa-map-marker"></i><span>{{$data->curent_location}}</span></li>
			</ul>
		</div>
	</div>
</div>
<div class="row bottom-mrg full-detail-description">
	<h2 class="detail-title">About Resume</h2>
	<p>{{$data->sek_dec}}</p>
</div>
<div class="row bottom-mrg full-detail-description">
	<h2 class="detail-title">Personal Information</h2>
	<ul class="detail-list">
		<li><span class="per_info_title">Father Name: </span> <span>{{$data->father_name}}</span></li>
		<li><span class="per_info_title">Mother Name: </span> <span>{{$data->mother_name}}</span></li>
		<li><span class="per_info_title">Address: </span> <span>{{$data->address}}</span></li>
		<li><span class="per_info_title">Nationality: </span> <span>{{$data->nationality}}</span></li>
		<li><span class="per_info_title">Current Location: </span> <span>{{$data->curent_location}}</span></li>
		<li><span class="per_info_title">Date of Birth: </span> <span>{{$data->birth_date}}</span></li>
		<li><span class="per_info_title">Gender: </span> <span>{{$data->gender}}</span></li>
		<li><span class="per_info_title">Marige Status: </span> <span>{{$data->marital_status}}</span></li>
	</ul>
</div>

@endsection