
@php
use App\Models\Notification;
use App\Models\employer_profile;
	$status=0;
	$notification_data= Notification::where('employer_id', auth()->user()->id)->where('status',$status)->get();
	$notification_number=$notification_data->count();

	$emp_data= employer_profile::where('user_id',auth()->user()->id)->first();
@endphp
<section class="user_dashbord">
	<div class="row ml_0 mr_0">
		<header class="job_seeker_header">
			<nav class="navbar navbar-toggleable-md navbar-light pt-0 pb-0 ">
				<div class="col-md-3">
					<div class="float-left">
						<a href="#" class="button-left">
							<span class="fa fa-fw fa-bars "></span>
						</a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="user-panel">
						<div class="user-panel_image detail-pic">
							<img src="{{url('/img/'.$emp_data->cmp_logo)}}" class="img" alt="" />
						</div>
						
						<div class="user-panel_info">
							<h1>{{auth()->user()->first_name}} {{auth()->user()->last_name}}</h1>
							<p>CEO</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<ul class="job_seeker_nav navbar-nav">
						<li class="nav-item dropdown notifications-menu">
							<a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="far fa-envelope"></i>
								<span class="label label-warning bg-warning">{{$notification_number}}</span>
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
								<ul class="dropdown-menu-over list-unstyled">
									<li class="job_seeker_header-ul text-center">
										You have {{$notification_number}} notifications
									</li>
									<li>
									<!-- inner menu: contains the actual data -->
										<ul class="menu list-unstyled">
											@foreach($notification_data as $notification_datas)
											<li>
												<a href="{{route('emp_notification_details', $notification_datas->id)}}">
													{{$notification_datas->job_title}}
												</a>
											</li>
											@endforeach
										</ul>
									</li>
									<li class="footer-ul text-center">
										<a href="#">View all</a>
									</li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
			</nav>
		</header>	
	</div>
</section>