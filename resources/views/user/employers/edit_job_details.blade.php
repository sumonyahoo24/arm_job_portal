@extends('user.employers.master')

@section('contant')
@foreach($emp_job_data as $emp_job)
<form action="{{route('create_job_process')}}" role="" method="post" enctype="multipart/form-data">
	@csrf
	<div class="row ptb_20 bb_2">
		<h2 class="detail-title">Job Details</h2>
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<input type="text" name="job_title" class="form-control" value="{{ $emp_job->job_title }}">
				@if($errors->has('job_title'))
				<span class="label label-danger">
					{{ $errors->first('job_title') }}
				</span>
				@endif
			</div>
			<div class="input-group">
				<input type="text" name="salary" class="form-control" value="{{ $emp_job->salary }}">
				@if($errors->has('salary'))
				<span class="label label-danger">
					{{ $errors->first('salary') }}
				</span>
				@endif
			</div>
			<div class="input-group logo_fprm date_form">
				<input type="date" name="dateline" class="form-control" value="{{ $emp_job->dateline }}">
			</div>	
		</div>
		
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<input type="text" name="vacancies" class="form-control" value="{{ $emp_job->vacancies }}">
				@if($errors->has('vacancies'))
				<span class="label label-danger">
					{{ $errors->first('vacancies') }}
				</span>
				@endif
			</div>
			<div class="input-group">
				<select name="job_level" class="form-control input-lg">
					<option value="{{ $emp_job->job_level }}">Job Level</option>
					<option value="Entry">Entry</option>
					<option value="Mid">Mid</option>
					<option value="Top">Top</option>
				</select>
			</div>
		</div>
		<textarea class="form-control textarea" name="job_desc" value="{{ $emp_job->job_desc }}"></textarea>
	</div>
	<div class="row ptb_20">
		<h2 class="detail-title">Candidates Requirements</h2>
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<input type="number" name="age" min="14" max="55" value="{{ $emp_job->age }}">
				@if($errors->has('age'))
				<span class="label label-danger">
					{{ $errors->first('age') }}
				</span>
				@endif
			</div>
			<div class="input-group">
				<textarea class="form-control textarea" name="additional_req" value="{{ $emp_job->additional_req }}"></textarea>
			</div>	
		</div>
		
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<select name="experience" class="form-control input-lg">
					<option value="{{ $emp_job->experience }}">Experience</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
				</select>
			</div>
			<div class="input-group">
				<textarea class="form-control textarea" name="other_beni" value="{{ $emp_job->other_beni }}"></textarea>
			</div>

			<div class="col-md-12 col-sm-12 mt_10">
				<button class="btn btn-success btn-primary mid_btn align_right">Update</button>	
			</div>		
		</div>
	</div>
	<div class="row ptb_20">	
		
	</div>
</form>
@endforeach
@endsection