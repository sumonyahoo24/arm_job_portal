@extends('user.employers.master')

@section('contant')
<form action="{{route('create_job_process')}}" role="" method="post" enctype="multipart/form-data">
	@csrf
	<div class="row ptb_20 bb_2">
		<h2 class="detail-title">Job Details</h2>
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<input type="text" name="job_title" class="form-control" placeholder="Job Title">
				@if($errors->has('job_title'))
				<span class="label label-danger">
					{{ $errors->first('job_title') }}
				</span>
				@endif
			</div>
			<div class="input-group">

				<select name="job_cate" class="form-control input-lg">
					@foreach($category as $categorys)
					<option>{{$categorys->cat_name}}</option>
					@endforeach
				</select>
				@if($errors->has('sek_categoory'))
                    <span class="label label-danger">
                        {{ $errors->first('sek_categoory') }}
                    </span>
                @endif
			</div>
			<div class="input-group">
				<input type="text" name="salary" class="form-control" placeholder="Salary">
				@if($errors->has('salary'))
				<span class="label label-danger">
					{{ $errors->first('salary') }}
				</span>
				@endif
			</div>
			<div class="input-group logo_fprm date_form">
				<input type="date" name="dateline" class="form-control" placeholder="Dateline">
			</div>	
		</div>
		
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<input type="text" name="vacancies" class="form-control" placeholder="No. of Vacancies">
				@if($errors->has('vacancies'))
				<span class="label label-danger">
					{{ $errors->first('vacancies') }}
				</span>
				@endif
			</div>
			<div class="input-group">
				<select name="location" class="form-control input-lg">
					@foreach($location as $locations)
					<option>{{$locations->location_name}}</option>
					@endforeach
				</select>
				@if($errors->has('location'))
                    <span class="label label-danger">
                        {{ $errors->first('location') }}
                    </span>
                @endif
			</div>
			<div class="input-group">
				<select name="job_level" class="form-control input-lg">
					<option value="">Job Level</option>
					<option value="Entry">Entry</option>
					<option value="Mid">Mid</option>
					<option value="Top">Top</option>
				</select>
			</div>
		</div>
		<textarea class="form-control textarea" name="job_desc" placeholder="Job Description"></textarea>
	</div>
	<div class="row ptb_20">
		<h2 class="detail-title">Candidates Requirements</h2>
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<select name="candi_degree" class="form-control input-lg">
					@foreach($qualification as $qualifications)
					<option>{{$qualifications->qualification_name}}</option>
					@endforeach
				</select>
				@if($errors->has('candi_degree'))
                    <span class="label label-danger">
                        {{ $errors->first('candi_degree') }}
                    </span>
                @endif
			</div>
			<div class="input-group">
				<input type="number" name="age" name="" min="14" max="55" placeholder="Age (e.g: 14-55)">
				@if($errors->has('age'))
				<span class="label label-danger">
					{{ $errors->first('age') }}
				</span>
				@endif
			</div>
			<div class="input-group">
				<textarea class="form-control textarea" name="additional_req" placeholder="Additional Requirements"></textarea>
			</div>	
		</div>
		
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<select name="experience" class="form-control input-lg">
					<option>Experience</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
				</select>
			</div>
			<div class="input-group">
				<input type="radio" class="form-control" name="gender" value="Male">
				<span class="g_name">Male</span> 
				<input type="radio" class="form-control" name="gender" value="Female"> 
				<span class="g_name">Female</span> 
				<input type="radio" class="form-control" name="gender" value="Other"> 
				<span class="g_name">Other</span> 
			</div>
			<div class="input-group">
				<textarea class="form-control textarea" name="other_beni" placeholder="Compensation & Other Benefits"></textarea>
			</div>

			<div class="col-md-12 col-sm-12 mt_10">
				<button class="btn btn-success btn-primary mid_btn align_right">Submit</button>	
			</div>		
		</div>
	</div>
	<div class="row ptb_20">	
		
	</div>
</form>
@endsection