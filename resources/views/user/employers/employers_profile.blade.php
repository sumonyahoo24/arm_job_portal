@extends('user.employers.master')

@section('contant')
<form action="{{route('employer_profile_process')}}" role="" method="post" enctype="multipart/form-data">
	@csrf
	<div class="row ptb_20 bb_2">
		<h2 class="detail-title">Upload Image</h2>
		<div class="form-group logo_fprm">
			<label for="exampleInputPhoto">Profile Photo</label>
			<input type="file" class="form-control" name="cmp_logo">
		</div>
		<h2 class="detail-title">Empolyers Details</h2>
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<label for="company_mame" class="wd_100">Company Name</label>
				<input type="text" name="cmp_name" id="company_mame" class="form-control" placeholder="Company Name">
				@if($errors->has('cmp_name'))
				<span class="label label-danger">
					{{ $errors->first('cmp_name') }}
				</span>
				@endif
			</div>
			<div class="input-group">
				<label for="cate_select" class="wd_100">Select Categorie</label>
				<select id="cate_select" name="cmp_categoory" class="form-control input-lg">
					<option value="Software">Software</option>
					<option value="Hardware">Hardware</option>
					<option value="Machanical">Machanical</option>
					<option value="HR/Manager">HR/Manager</option>
				</select>
			</div>
			<div class="input-group">
				<label for="city_select" class="wd_100">Select City</label>
				<select id="city_select" name="cmp_city" class="form-control input-lg">
					<option value="Dhaka">Dhaka</option>
					<option value="chittagong">chittagong</option>
					<option value="Kushtia">Kushtia</option>
					<option value="Pabna">Pabna</option>
				</select>
			</div>	
		</div>
		
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<label for="company_tag" class="wd_100">Tagline</label>
				<input type="text" id="company_tag" name="cmp_tagline" class="form-control" placeholder="Company Tagline">
				@if($errors->has('cmp_tagline'))
				<span class="label label-danger">
					{{ $errors->first('cmp_tagline') }}
				</span>
				@endif
			</div>
			<div class="input-group">
				<label for="designation" class="wd_100">Designation</label>
				<input type="text" id="designation" name="cnt_designation" class="form-control" placeholder="Company CEO Name">
				@if($errors->has('cnt_designation'))
				<span class="label label-danger">
					{{ $errors->first('cnt_designation') }}
				</span>
				@endif
			</div>	
		</div>
	</div>
	<div class="row ptb_20 bb_2">
		<h2 class="detail-title">Information</h2>
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
				<span class="em_profile_comon">{{auth()->user()->email}}</span>
			</div>
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-phone"></i></span>
				<span class="em_profile_comon">{{auth()->user()->phone}}</span>
			</div>
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-globe"></i></span>
				<input type="text" name="cmp_website" class="form-control" placeholder="Website Address">
				@if($errors->has('cmp_website'))
				<span class="label label-danger">
					{{ $errors->first('cmp_website') }}
				</span>
				@endif
			</div>	
		</div>
		
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
				<input type="text" name="cmp_address" class="form-control" placeholder="Local E.g. It Park New">
				@if($errors->has('cmp_address'))
				<span class="label label-danger">
					{{ $errors->first('cmp_address') }}
				</span>
				@endif
			</div>
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-users"></i></span>
				<input type="text" class="form-control" name="employe_rang" placeholder="Employee E.g. 100-2500" />
				@if($errors->has('employe_rang'))
				<span class="label label-danger">
					{{ $errors->first('employe_rang') }}
				</span>
				@endif
			</div>	
		</div>
	</div>
	<div class="row ptb_20">
		<h2 class="detail-title">About Company</h2>
		
		<div class="col-md-12 col-sm-12">
			<textarea class="form-control textarea" name="cmp_dec" placeholder="About Company"></textarea>
		</div>	
		<div class="col-md-12 col-sm-12 mt_10">
			<button class="btn btn-success btn-primary mid_btn">Update Profile</button>	
		</div>	
	</div>
</form>
@endsection