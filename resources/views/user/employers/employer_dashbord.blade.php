@extends('user.employers.master')

@section('contant')
	<div class="row bottom-mrg">
        <div class="col-md-12 col-sm-12">
            <div class="advance-detail detail-desc-caption">
                <ul>
                    <li><strong class="j-applied">{{$job_count}}</strong>Job Posts</li>
                    <li><strong class="j-shared">{{$app_count}}</strong>Aplications</li>
                </ul>
            </div>
        </div>
    </div>
@endsection
