@extends('user.master')

@section('contant')

<div class="row mrg-0">
	<div class="detail-pic">
		<img src="img/ind1.jpg" class="img" alt="" />
	</div>
</div>
<div class="row bottom-mrg col_text_center">
	<div class="col-md-8">
		<form>
			<div class="form-group">
				<label for="name">Name</label>
				<input type="text" class="form-control" id="name" placeholder="Enter Name">
			</div>
			<div class="form-group">
				<label for="designation">Designation</label>
				<input type="text" class="form-control" id="designation" placeholder="Designation">
			</div>
			<button type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>

	<div class="col-md-8 col-sm-8">
		<div class="detail-desc-caption">
			<label for="name">Name</label>
			<input id="name" type="text">
			<span class="designation">Web Developer</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
		</div>
		<div class="detail-desc-skill">
			<span>HTML</span><span>css</span><span>photoshop</span>
			<span>java</span><span>php</span><span>bootstrap</span>
		</div>
	</div>
	<div class="col-md-4 col-sm-4">
		<div class="get-touch">
			<h4>Get in Touch</h4>
			<ul>
				<li><i class="fa fa-map-marker"></i><span>Menlo Park, CA</span></li>
				<li><i class="fa fa-envelope"></i><span>danieldax704@gmail.com</span></li>
				<li><i class="fa fa-phone"></i><span>0 123 456 7859</span></li>
				<li><i class="fa fa-money"></i><span>$52/Hour</span></li>
			</ul>
		</div>
	</div>
</div>
<div class="row bottom-mrg full-detail-description">
	<h2 class="detail-title">About Resume</h2>
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
</div>

<div class="row bottom-mrg full-detail-description">
	<h2 class="detail-title">Education</h2>
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
	<ul class="detail-list">
		<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</li>
		<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</li>
		<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</li>
		<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</li>
		<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</li>
		<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do.</li>
		<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do.</li>
		<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
		<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
		<li>Lorem ipsum dolor sit amet, consectetur.</li>
	</ul>
</div>
<div class="row bottom-mrg full-detail-description">
	<h2 class="detail-title">Personal Information</h2>
	<ul class="detail-list">
		<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</li>
	</ul>
</div>
<div class="row bottom-mrg full-detail-description">
	<h2 class="detail-title">Work Experience</h2>
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
	<ul class="detail-list">
		<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</li>
		<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</li>
		<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</li>
		<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</li>
		<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</li>
		<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do.</li>
		<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do.</li>
		<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
		<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
		<li>Lorem ipsum dolor sit amet, consectetur.</li>
	</ul>
</div>

@endsection