@extends('admin.master')

@section('contant')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="user_list_btn">
                    <a href="{{route('job_seeker_list')}}">
                        <button type="button" class="btn btn-success">Job Seeker List</button>
                    </a>
                    <a href="{{route('user_list')}}">
                        <button type="button" class="btn btn-success">User List</button>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="user_list_header">
                        <h4 class="title">Job Seeker List</h4>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover table-striped">
                            <thead class="user_list_head">
                                <th class="single_user_head">Name</th>
                                <th class="single_user_head">Email</th>
                                <th class="single_user_head">Phone</th>
                                <th class="single_user_head">User Name</th>
                                <th class="single_user_head">User Type</th>
                                <th class="single_user_head">Date</th>
                            </thead>
                            <tbody>
                                @foreach($employers_list as $employers_lists)
                                <tr>
                                    <td>{{$employers_lists->first_name}} {{$employers_lists->last_name}}</td>
                                    <td>{{$employers_lists->email}}</td>
                                    <td>{{$employers_lists->phone}}</td>
                                    <td>{{$employers_lists->user_name}}</td>
                                    <td>{{$employers_lists->user_type}}</td>
                                    <td>{{$employers_lists->created_at}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
