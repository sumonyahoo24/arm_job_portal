@extends('admin.master')

@section('contant')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="user_list_header">
                        <h4 class="title">Job List</h4>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover table-striped">
                            <thead class="user_list_head">
                                <th class="single_user_head">Title</th>
                                <th class="single_user_head">Salary</th>
                                <th class="single_user_head">Vacancy</th>
                                <th class="single_user_head">Location</th>
                                <th class="single_user_head">Dateline</th>
                            </thead>
                            <tbody>
                                @foreach($Job_list_admin as $Job_lists_admin)
                                <tr>
                                    <td>{{$Job_lists_admin->job_title}}</td>
                                    <td>{{$Job_lists_admin->salary}}</td>
                                    <td>{{$Job_lists_admin->vacancies}}</td>
                                    <td>{{$Job_lists_admin->location}}</td>
                                    <td>{{$Job_lists_admin->dateline}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
