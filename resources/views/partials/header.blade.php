<!--===========Top_Bar Start Here===========-->
<section class="top-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                @guest
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary btn-lg modal_btn" data-toggle="modal" data-target="#exampleModalCenter">
                  Login / Register
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Are You Want To Register or Login</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                                
                                <ul class="login_modal">
                                    <div class="login_select">
                                        <li class="single_login">
                                            <ul class="al_sin_log">
                                                <li class="single_login_left">
                                                    <i class="fas fa-user-graduate"></i>
                                                </li>
                                                <li class="single_login_right">
                                                    <div class="t_txt">
                                                        <h1>
                                                            Job Seeker
                                                        </h1>
                                                    </div>
                                                    <div class="des_txt">Sign in or create your My Bdjobs account to manage your profile</div>
                                                    <div class="btn-wraper">
                                                        <a href="{{route('login')}}" class="btn btn-primary common_btn log_btn_lt">Sign in</a>
                                                        <a href="{{route('registration')}}" class="btn btn-primary common_btn log_btn_rt">Create Account</a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                         
                                        <li class="single_login">
                                            <ul class="al_sin_log">
                                                <li class="single_login_left">
                                                    <i class="fas fa-people-carry"></i>
                                                </li>
                                                <li class="single_login_right">
                                                    <div class="t_txt">
                                                            <h1>
                                                                Employers
                                                            </h1>
                                                        </div>
                                                    <div class="des_txt">Sign in or create account to find the best candidates in the fastest way</div>
                                                    <div class="btn-wraper">
                                                        <a href="{{route('login')}}" class="btn btn-primary common_btn">Sign in</a>
                                                        <a href="{{route('registration')}}" class="btn btn-primary common_btn">Create Account</a>
                                                    </div>
                                                </li>
                                            </ul>
                                                
                                        </li>
                                    </div>
                                </ul>


                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-primary modal_btn" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                <nav class="navbar navbar-expand-lg">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav log_nav">
                            <li class="nav-item dropdown">
                                <a class="nav-link log_m_nav m_nav dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ auth()->User()->first_name }} {{ auth()->User()->last_name }}
                                </a>
                                <div class="dropdown-menu log_drop_nav" aria-labelledby="navbarDropdownMenuLink">
                                    <ul>
                                        <li>
                                            <a class="dropdown-item" href="{{url('/'.auth()->user()->user_type)}}">Dashboard</a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="{{route('logout')}}">Log Out</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                @endguest
            </div>
            <div class="col-md-6">
                <ul class="top_social_all align_right">
                    <li class="top_social_single">
                        <a href="#" class="top_social_link">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li class="top_social_single">
                        <a href="#" class="top_social_link">
                            <i class="fab fa-youtube"></i>
                        </a>
                    </li>
                    <li class="top_social_single">
                        <a href="#" class="top_social_link">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                    <li class="top_social_single">
                        <a href="#" class="top_social_link">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--===========Top_Bar End Here===========-->
<!--===========Menu Start Here===========-->
<section class="main_menu_bar">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <nav class="navbar">
                    <a class="navbar-brand" href="{{route('index')}}">
                        <img src="img/logo.png" width="100" height="50" alt="Jobs">
                    </a>
                </nav>
            </div>
            <div class="col-md-9">
                <nav class="navbar navbar-expand-lg align_right">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav">
                            <li class="nav-item active">
                                <a class="nav-link m_nav" href="{{ route('index') }}">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link m_nav" href="{{route('jobs')}}">Jobs</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link m_nav" href="{{ route('contact_us') }}">Contact us</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</section>
<!--===========Menu End Here===========-->