<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcademicQualificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academic_qualifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sekker_id')->nullable();
            $table->string('title', 32)->nullable();
            $table->string('major', 32)->nullable();
            $table->string('institute', 32)->nullable();
            $table->float('result', 8, 2)->nullable();
            $table->integer('scale')->nullable();
            $table->integer('year')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('academic_qualifications');
    }
}
