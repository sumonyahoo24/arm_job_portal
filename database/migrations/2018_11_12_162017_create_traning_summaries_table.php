<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTraningSummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traning_summaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seeker_id')->unsigned();
            $table->string('title', 64)->nullable();
            $table->string('institute', 32)->nullable();
            $table->string('location', 32)->nullable();
            $table->string('duration', 32)->nullable();
            $table->string('topic',32)->nullable();
            $table->string('country', 32)->nullable();
            $table->integer('year')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('traning_summaries');
    }
}
