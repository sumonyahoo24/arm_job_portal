<?php

namespace App\Http\Controllers\Frontend\Job_Seeker;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\seeker_profile;
use App\Models\academic_qualification;
use App\Models\traning_summary;
use App\Models\Applied_Jobs;
use App\Models\Notification;
use App\Models\new_job;
use App\Models\Category;
use App\Models\Location;
use App\Models\Academic_Qualification_Admin;
use App\Models\employer_profile;
use App\Models\User;

class JobSeekerController extends Controller
{

    public function academic_qua(){

        $qualification= Academic_Qualification_Admin::all();
    //dd($notification_data);
        return view('user.job_seekers.accademic_qualification', compact('qualification'));
    }

    public function create_profile(){

        $category= Category::all();
        $location= Location::all();
        //dd($category);

    	return view('user.job_seekers.create_profile', compact('category', 'location'));
    }

    public function job_seeker(){

         $status=0;
        $notification_data= Notification::where('sekker_id', auth()->user()->id)->where('status',$status)->get();
        $notification_number=$notification_data->count();
        $seeker= seeker_profile::where('user_id',auth()->user()->id)->first();

        $category= seeker_profile::where('user_id',auth()->user()->id)->first();

        $seeker_category=($category->sek_categoory);

        $all_jobs= new_job::where('job_cate', $seeker_category)->get();

        $job_cate=$all_jobs->count();

        $job_seeker_id= Applied_Jobs::where('sekker_id',auth()->user()->id)->get();
        $job_seeker_job_count=$job_seeker_id->count();
        //dd($job_seeker_job_count);

    	return view('user.job_seekers.job_seeker', compact('job_cate', 'job_seeker_job_count', 'notification_number'));
    }

    public function view_resume(){

        $data= seeker_profile::where('user_id',auth()->user()->id)->first();

        $seeker= seeker_profile::all()->where('user_id', auth()->user()->id)->first();

        $seeker_id_take= ($seeker->id);

        $education_data=academic_qualification::where('sekker_id',$seeker_id_take)->get();
        $traning_data=traning_summary::where('seeker_id',$seeker_id_take)->get();
        //dd($traning_data);
      
    	return view('user.job_seekers.resume_details', compact('data','education_data','traning_data'));
    }

    public function edit_resume(){

    	return view('user.job_seekers.edit_resume');
    }

    public function train_summary(){

        $location= Location::all();

        return view('user.job_seekers.training_summary', compact('location'));
    }

    public function seeker_applied_jobs(){

        $seeker_id=(auth()->user()->id);
        //dd($seeker_id);
        $seeker_application_data=Applied_Jobs::where('sekker_id',$seeker_id)->get();
        return view('user.job_seekers.seeker_applied_jobs', compact('seeker_application_data'));
    }

    public function emp_datils_by_application($employer_id){

        
        
        $emp_data=employer_profile::where('user_id',$employer_id)->first();
        //$emp_user_data=User::where('id',$employer_id)->get();
        //dd($emp_user_data);
        return view('user.job_seekers.emp_datils_by_application', compact('emp_data', 'emp_user_data'));
    }

    public function notification_details($id){

        $input=1;

        $notification=Notification::where('id',$id)->first();

        Notification::where('id', $id)->Update([
            
            'status'=>$input,

        ]);

        return view('user.job_seekers.notification_details', compact('notification'));
    }

    public function create_profile_process(Request $request)
    {
        //Get All Input Data

        $inputs = $request->except('_token');

        $validator = Validator::make($inputs, [

             'father_name' => 'required',
             'mother_name' => 'required',
             'curent_location' => 'required',
             'address' => 'required', 
             'nationality' => 'required', 
             'sek_categoory' => 'required', 
             'sek_deg' => 'required', 
             'profile_photo' => 'required', 
        ]);

         if ($validator->fails()) {
             return redirect()->back()->withErrors($validator)->withInput();
         }




        // save the file
        $profile_photo = $request->file('profile_photo');

        $file_name=uniqid("photo_",true).str_random(10).'.'.$profile_photo->getClientOriginalExtension();
         // dd($file_name);
            if($profile_photo->isValid()){
               
                $profile_photo->storeAs('img',$file_name);
            }

        //dd($profile_photo);
        //$path = $profile_photo->store('profile_photo');


        // create user
        /*$check =seeker_profile::select('user_id');*/

        $auth_id= auth()->user()->id;

        seeker_profile::where('user_id', $auth_id )->Update([
            'father_name'=>trim($request->input('father_name')),
            'mother_name' => trim($request->input('mother_name')),
            'curent_location' => trim($request->input('curent_location')),
            'address' => trim($request->input('address')),
            'nationality' => trim($request->input('nationality')),
            'birth_date' => trim($request->input('birth_date')),
            'sek_categoory' => trim($request->input('sek_categoory')),
            'sek_deg' => trim($request->input('sek_deg')),
            'marital_status' => trim($request->input('marital_status')),
            'gender' => trim($request->input('gender')),
            'sek_dec' => trim($request->input('sek_dec')),
            'profile_photo' => $file_name,

        ]);
// $seeker= seeker_profile::all();
// dd($seeker);

//dd($seeker);
        // $seeker= seeker_profile::all()->first();
        // //dd($seeker);
        // academic_qualification::create([
        //         'sekker_id'=>($seeker->id) ,
        //     ]);
        //dd('seeker_profile');

        session()->flash('message', 'Profile Update successfully.');
        return redirect()->route('academic_qua');


    }


    public function accademic_qualification_process(Request $request)
    {
                //Get All Input Data

        $inputs = $request->except('_token');

        $validator = Validator::make($inputs, [

             'title' => 'required',
             'major' => 'required',
             'institute' => 'required',
             'result' => 'required',
             'scale' => 'required',     
             'year' => 'required',
        ]);

         if ($validator->fails()) {
             return redirect()->back()->withErrors($validator)->withInput();
         }


        // create user
        /*$check =seeker_profile::select('user_id');*/
        $check_title=($request->input('title'));

        $seeker= seeker_profile::all()->where('user_id', auth()->user()->id)->first();

        $seeker_id_take= ($seeker->id);
        //$seeker_id_take= seeker_profile::select(['id'])->where('user_id', auth()->user()->id)->first();
        $check_academic_titles= academic_qualification::select(['title'])->where('sekker_id', $seeker_id_take)->where('title', $check_title)->first();
//dd($check_academic_titles);

        if (!empty($check_academic_titles)) {

                $test=academic_qualification::where('sekker_id', $seeker_id_take)->where('title',$check_title)->Update([
                    
                    'title'=>trim($request->input('title')),
                    'major' => trim($request->input('major')),
                    'institute' => trim($request->input('institute')),
                    'result' => trim($request->input('result')),
                    'scale' => trim($request->input('scale')),
                    'year' => trim($request->input('year')),

                ]);
//dd($test);
                session()->flash('message', 'Accademic Qualification Update successfully.');
                return redirect()->route('train_summary');
        }
        else
        {
                $seeker= seeker_profile::all()->where('user_id', auth()->user()->id)->first();

                academic_qualification::create([
                    'sekker_id'=>($seeker->id),
                    'title'=>trim($request->input('title')),
                    'major' => trim($request->input('major')),
                    'institute' => trim($request->input('institute')),
                    'result' => trim($request->input('result')),
                    'scale' => trim($request->input('scale')),
                    'year' => trim($request->input('year')),

                ]);
        //dd($seeker);

                session()->flash('message', 'Accademic Qualification added successfully.');
                return redirect()->route('train_summary');
        }

    }


    public function training_summary_process(Request $request)
    {
        //Get All Input Data

        $inputs = $request->except('_token');

        $validator = Validator::make($inputs, [

             'title' => 'required',
             'location' => 'required',
             'institute' => 'required',
             'duration' => 'required',
             'topic' => 'required',     
             'year' => 'required',
             'country' => 'required',
        ]);

         if ($validator->fails()) {
             return redirect()->back()->withErrors($validator)->withInput();
         }


        // create user
        /*$check =seeker_profile::select('user_id');*/
        $check_traning_in=($request->input('title'));
//dd($check_traning_in);
        $seeker= seeker_profile::all()->where('user_id', auth()->user()->id)->first();

        $seeker_id_take= ($seeker->id);

        $check_traning_titles= traning_summary::select(['title'])->where('seeker_id', $seeker_id_take)->where('title',$check_traning_in)->first();


        if (!empty($check_traning_titles)) {

                traning_summary::where('seeker_id', $seeker_id_take)->where('title', $check_traning_in)->Update([
                    
                    'title'=>trim($request->input('title')),
                    'institute' => trim($request->input('institute')),
                    'location' => trim($request->input('location')),
                    'duration' => trim($request->input('duration')),
                    'topic' => trim($request->input('topic')),
                    'country' => trim($request->input('country')),
                    'year' => trim($request->input('year')),

                ]);

                session()->flash('message', 'Accademic Qualification Update successfully.');
                return redirect()->route('view_resume');
        }
        else
        {
                $seeker= seeker_profile::all()->where('user_id', auth()->user()->id)->first();

                traning_summary::create([
                    'seeker_id'=>($seeker->id),
                    'title'=>trim($request->input('title')),
                    'institute' => trim($request->input('institute')),
                    'location' => trim($request->input('location')),
                    'duration' => trim($request->input('duration')),
                    'topic' => trim($request->input('topic')),
                    'country' => trim($request->input('country')),
                    'year' => trim($request->input('year')),

                ]);
        //dd($seeker);

                session()->flash('message', 'Accademic Qualification added successfully.');
                return redirect()->route('view_resume');
        }
    }


}
