<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\new_job;
use App\Models\Category;
use App\Models\location;
use App\Models\Academic_Qualification_Admin;
use App\Models\Notification;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function admin_dashbord()
    {
        $status=0;
        $notification_data= Notification::where('admin_status',$status)->get();
        $notification_number=$notification_data->count();

    	$jobs= new_job::all();
    	$jobs_count= $jobs->count();

    	//dd($jobs_count);
    	$user= User::where('user_type', 'job_seeker')->get();
    	$user_count= $user->count();

        $todaysJob= new_job::whereDate('updated_at', Carbon::today())->count();

        //dd($todaysJob);

        
    	return view('admin.dashboard', compact('user_count', 'jobs_count', 'notification_data', 'notification_number', 'todaysJob'));
    }

    public function addmin_notification_details($id)
    {
        $status=0;
        $notification_data= Notification::where('admin_status',$status)->get();

        $notification_data_single= Notification::where('id', $id)->where('admin_status',$status)->get();

        //dd($notification_data);
        $notification_number=$notification_data->count();

         $input=1;

        Notification::where('id', $id)->Update([
            
            'admin_status'=>$input,

        ]);

        return view('admin.addmin_notification_details', compact('notification_data', 'notification_number', 'notification_data_single'));
    }

    public function user_list()
    {
        $status=0;
        $notification_data= Notification::where('admin_status',$status)->get();
        $notification_number=$notification_data->count();

        $user_list= User::all();
        //dd($user_list);
        return view('admin.user_list', compact('user_list', 'notification_data', 'notification_number'));
    }

    public function Job_list_admin()
    {
         $status=0;
        $notification_data= Notification::where('admin_status',$status)->get();
        $notification_number=$notification_data->count();

        $Job_list_admin= new_job::all();
        //dd($user_list);
        return view('admin.Job_list_admin', compact('Job_list_admin', 'notification_data', 'notification_number'));
    }

    public function Job_list_status_active($id)
    {

        $Job_list_admin= new_job::all();
        //dd($user_list);
        return view('admin.Job_list_admin', compact('Job_list_admin'));
    }

    public function Job_list_status_inactive($id)
    {
    	$Job_list_admin= new_job::all();
    	//dd($user_list);
    	return view('admin.Job_list_admin', compact('Job_list_admin'));
    }

    public function job_seeker_list()
    {
         $status=0;
        $notification_data= Notification::where('admin_status',$status)->get();
        $notification_number=$notification_data->count();

    	$job_seeker_list= User::where('user_type', 'job_seeker')->get();
    	//dd($job_seeker_list);
    	return view('admin.job_seeker_list', compact('job_seeker_list', 'notification_data', 'notification_number'));
    }

    public function employers_list()
    {
         $status=0;
        $notification_data= Notification::where('admin_status',$status)->get();
        $notification_number=$notification_data->count();

    	$employers_list= User::where('user_type', 'employer')->get();
    	//dd($employers_list);
    	return view('admin.employers_list', compact('employers_list', 'notification_data', 'notification_number'));
    }

    public function categories()
    {
        $status=0;
        $notification_data= Notification::where('admin_status',$status)->get();
        $notification_number=$notification_data->count();

        $cat_list= Category::all();
        //dd($cat_list);
        return view('admin.categories', compact('cat_list', 'notification_data', 'notification_number'));
    }

    public function accademic_qualification_admin()
    {
        $status=0;
        $notification_data= Notification::where('admin_status',$status)->get();
        $notification_number=$notification_data->count();

    	$qualification_list= Academic_Qualification_Admin::all();
    	//dd($qualification_list);
    	return view('admin.accademic_qualification_admin', compact('qualification_list', 'notification_data', 'notification_number'));
    }





    public function add_category_process(Request $request)
    {
    	$inputs = $request->except('_token');

    	$cat_input= ($request->input('cat_name'));
    	$cat_check= Category::select(['cat_name'])->where('cat_name', $cat_input)->first();
    	//dd($cat_check);
    	if (!empty($cat_check)) {
    		
    	
       Category::where('cat_name', $cat_check)->Update([

            'cat_name'=>trim($request->input('cat_name')),

        ]);
        //dd($employers);

        session()->flash('message', 'Category Update successfully.');
        return redirect()->back();
    }
    else
    {
    	Category::create([

            'cat_name'=>trim($request->input('cat_name')),

        ]);
        //dd($employers);

        session()->flash('message', 'Category added successfully.');
        return redirect()->back();
    }

    }

    public function edit_category($id)
    {
        $status=0;
        $notification_data= Notification::where('admin_status',$status)->get();
        $notification_number=$notification_data->count();

    	$data= Category::find($id);
    	//dd($data);
    	// $cat_lists= Category::all();
    	// dd($cat_lists);
    	return view('admin.edit_category', compact('data', 'notification_data', 'notification_number'));
    }

    public function edit_category_process($id, Request $request)
    {
    	$data= Category::find($id);

    	$data->Update([

            'cat_name'=>trim($request->input('cat_name')),

        ]);
        //dd($employers);

        session()->flash('message', 'Category Updated successfully.');
        return redirect()->route('categories');

    }

    public function delete_category_process($id)
    {
        $data= Category::find($id);
        //dd($data);
        $data->delete();

        session()->flash('message', 'Category Deleted successfully.');
        return redirect()->route('categories');
    }

    public function delete_location_process($id)
    {
        $data= location::find($id);
        //dd($data);
        $data->delete();

        session()->flash('message', 'Location Deleted successfully.');
        return redirect()->route('locations');
    }

    public function delete_qualification_process($id)
    {
    	$data= Academic_Qualification_Admin::find($id);
    	//dd($data);
    	$data->delete();

    	session()->flash('message', 'Qualification Deleted successfully.');
        return redirect()->route('accademic_qualification_admin');
    }


    public function locations()
    {
        $status=0;
        $notification_data= Notification::where('admin_status',$status)->get();
        $notification_number=$notification_data->count();

    	$locations_list= Location::all();
    	//dd($cat_list);
    	return view('admin.locations', compact('locations_list', 'notification_data', 'notification_number'));
    }


    public function add_location_process(Request $request)
    {
        $inputs = $request->except('_token');

        $location_input= ($request->input('location_name'));
        $location_check= Location::select(['location_name'])->where('location_name', $location_input)->first();
        //dd($cat_check);
        if (!empty($location_check)) {
            
        
           Location::where('location_name', $location_check)->Update([

                'location_name'=>trim($request->input('location_name')),

            ]);
            //dd($employers);

            session()->flash('message', 'Location Update successfully.');
            return redirect()->back();
        }
        else
        {
            Location::create([

                'location_name'=>trim($request->input('location_name')),

            ]);
            //dd($employers);

            session()->flash('message', 'Location added successfully.');
            return redirect()->back();
        }

    }


    public function add_qualification_process(Request $request)
    {
    	$inputs = $request->except('_token');

    	$qualification_input= ($request->input('qualification_name'));
    	$qualification_check= Academic_Qualification_Admin::select(['qualification_name'])->where('qualification_name', $qualification_input)->first();
    	//dd($cat_check);
    	if (!empty($qualification_check)) {
    		
    	
           Academic_Qualification_Admin::where('qualification_name', $qualification_check)->Update([

                'qualification_name'=>trim($request->input('qualification_name')),

            ]);
            //dd($employers);

            session()->flash('message', 'Academic Qualification Update successfully.');
            return redirect()->back();
        }
        else
        {
        	Academic_Qualification_Admin::create([

                'qualification_name'=>trim($request->input('qualification_name')),

            ]);
            //dd($employers);

            session()->flash('message', 'Academic Qualification added successfully.');
            return redirect()->back();
        }

    }

    public function edit_qualification($id)
    {
        $status=0;
        $notification_data= Notification::where('admin_status',$status)->get();
        $notification_number=$notification_data->count();

        $data= Academic_Qualification_Admin::find($id);
        return view('admin.edit_qualification', compact('data', 'notification_data', 'notification_number'));
    }

    public function edit_qualification_process($id, Request $request)
    {
        $data= Academic_Qualification_Admin::find($id);

        $data->Update([

            'qualification_name'=>trim($request->input('qualification_name')),

        ]);
        //dd($employers);

        session()->flash('message', 'Qualification Updated successfully.');
        return redirect()->route('accademic_qualification_admin');

    }


    public function edit_location($id)
    {
        $status=0;
        $notification_data= Notification::where('admin_status',$status)->get();
        $notification_number=$notification_data->count();

    	$data= Location::find($id);
    	return view('admin.edit_location', compact('data', 'notification_data', 'notification_number'));
    }

    public function edit_location_process($id, Request $request)
    {
        $data= Location::find($id);

        $data->Update([

            'location_name'=>trim($request->input('location_name')),

        ]);
        //dd($employers);

        session()->flash('message', 'Location Updated successfully.');
        return redirect()->route('locations');

    }

    public function edit_user_process($id)
    {
        echo "bobo";
//     	$data= User::find($id);
// dd($data);
//     	$data->Update([

//             'first_name'=>trim($request->input('first_name')),
//             'last_name'=>trim($request->input('last_name')),
//             'user_name'=>trim($request->input('user_name')),
//             'user_name'=>trim($request->input('phone')),

//         ]);
//         //dd($employers);

//         session()->flash('message', 'User Updated successfully.');
//         return redirect()->route('user_list');

    }

    public function edit_user_admin($id)
    {
        $status=0;
        $notification_data= Notification::where('admin_status',$status)->get();
        $notification_number=$notification_data->count();

        $data= User::find($id);
        return view('admin.edit_user_admin', compact('data', 'notification_data', 'notification_number'));
    }

    public function admin_edit_user_process($id, Request $request) 
    {

        $data= User::find($id);

        $data->Update([

            'first_name'=>trim($request->input('first_name')),
            'last_name'=>trim($request->input('last_name')),
            'user_name'=>trim($request->input('user_name')),
            'phone'=>trim($request->input('phone')),

        ]);
        //dd($employers);

        session()->flash('message', 'User Updated successfully.');
        return redirect()->route('user_list');
    }
}
