<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'job_id','employer_id','sekker_id','job_title','salary','first_name','last_name','sec_deg','exp_salary','message','status','admin_status',
    ];
}
