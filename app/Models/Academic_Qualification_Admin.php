<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Academic_Qualification_Admin extends Model
{
        protected $fillable = [
        'qualification_name',
    ];
}
